TARGET_PATH = public

default: $(TARGET_PATH)/index.html

$(TARGET_PATH)/stone.css: stone.css
	@mkdir -p $(TARGET_PATH)
	@cp -v $< $@

$(TARGET_PATH)/index.html: index.txt $(TARGET_PATH)/stone.css _template.html Makefile
	@mkdir -p $(TARGET_PATH)
	pandoc \
		$< \
		--from=markdown+auto_identifiers \
		--to=html5 \
		--output=$@ \
		--standalone \
		--template=_template.html \
		--email-obfuscation=references \
		--verbose

clean:
	@rm -rfv $(TARGET_PATH)
